﻿mergeInto(LibraryManager.library,
    {
        SendEventInitialize : function(api_Key, api_Url)
        {
            var analytics =  window.analytics;
            console.log(Pointer_stringify(api_Url));
            analytics.initialize({
            'Segment.io': {
                    apiKey: "", // your app key
                    apiUrl: Pointer_stringify(api_Url) // refer quip doc https://gofynd.quip.com/FIo1AF4dgwBn/Fynd-Platform-ETL-for-Analytics-Events
                }
            });
        },

        SendGameAnalytics : function(event_Name,user_Id,game_Id,property_Data)
        {
            var analytics =  window.analytics;
            if(analytics != null)
            {
                console.log(Pointer_stringify(property_Data));
                analytics.track(Pointer_stringify(event_Name), {
                    'userId' : Pointer_stringify(user_Id),
                    'gameId' : Pointer_stringify(game_Id),
				    'propertyData' : JSON.parse(Pointer_stringify(property_Data))
                });
            }
            else
            {
                console.log("Analytics object null"); 
            }
			
        },
        
        SetIdentity : function(id, emailId, userName, enableAllDestination)
        {
            var analytics =  window.analytics;
            analytics.identify(id, {
              emailId: emailId
            },
            {
                  integrations: {
                       'All':enableAllDestination
                 }
            });
        }
    });