mergeInto(LibraryManager.library, {
  SendUnityLoadedMessage: function (message) {
    if (window.__frolic) {
      window.__frolic['onGameLoad'](Pointer_stringify(message));
    }
  },

  SendAPIVariablesErrorCode: function (errorCode) {
    var errorCodeString = Pointer_stringify(errorCode);
    if (window.__frolic) {
      window.__frolic['onMissingAPIVariables'](errorCodeString);
    }
    window.ShowErrorCode(errorCodeString);
  },

  SendPlayerInfoErrorCode: function (errorCode) {
    if (window.__frolic) {
      window.__frolic['onMissingPlayerVariables'](Pointer_stringify(errorCode));
    }
  },

  SendConnectionStatus: function (status) {
    window.UpdateConnectionStatus(status);
  },

  SendPlayerConnectedToGameMessage: function (message) {
    window.executePlayerConnectedToGame('onPlayerConnected', Pointer_stringify(message))
  },

  SendGameplaySceneLaunchedMessage: function (message) {
    if (window.__frolic) {
      window.__frolic['onGameStarted'](Pointer_stringify(message));
    }
  },

  SendGameplayStartedMessage: function (message) {
    window.executeGameplayStarted('onGameplayStarted', Pointer_stringify(message))
  },

  SendGameEndMessage: function (message) {
    if (window.__frolic) {
      window.__frolic['onGameFinished'](Pointer_stringify(message));
    }
  },
});
