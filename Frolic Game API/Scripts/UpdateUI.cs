using UnityEngine;
using UnityEngine.UI;

namespace FrolicGamesAPI
{
  public class UpdateUI : MonoBehaviour
  {
    [Header("SinglePlayerMode UI elements")]
    [SerializeField] private Text sp_localPlayer_Name_Text;
    [SerializeField] private Text sp_localPlayer_Score_Text;
    [SerializeField] private Image sp_localPlayer_Avatar_Image;
    [SerializeField] private Text sp_timer_Text;

    [Header("MultiPlayerMode UI elements")]
    [SerializeField] private Text mp_localPlayer_Name_Text;
    [SerializeField] private Text mp_localPlayer_Score_Text;
    [SerializeField] private Image mp_localPlayer_Avatar_Image;
    [SerializeField] private Text opponent_Name_Text;
    [SerializeField] private Text opponent_Score_Text;
    [SerializeField] private Image opponent_Avatar_Image;
    [SerializeField] private Text mp_timer_Text;

    [Space]
    [SerializeField] GameObject singleplayer_heartPanel;
    [SerializeField] GameObject multiplayer_heartPanel;

    [Space]
    [SerializeField] private GameObject gameOverPanel;
    [SerializeField] private GameObject singleplayerPanel;
    [SerializeField] private GameObject multiplayerPanel;

    [Space]
    [SerializeField] GameObject singlePlayerFinalScorePanel;
    [SerializeField] private Text sp_localplayer_FinalScore_Text;

    void Start()
    {
      CountDownTimer.Instance.getCurrentTime += UpdateTimer;

      PlayerUI_InfoUpdater.Instance.getOpponentScore += UpdateEnemyScore;

      PlayerUI_InfoUpdater.Instance.getLocalPlayer_Name += UpdateYourName;
      PlayerUI_InfoUpdater.Instance.getLocalPlayer_Avatar += UpdateYourAvatar;

      PlayerUI_InfoUpdater.Instance.getOpponentPlayer_Name += UpdateEnemyName;
      PlayerUI_InfoUpdater.Instance.getOpponentPlayer_Avatar += UpdateEnemyAvatar;

      OpenGameModePanel();// Show game mode panel (singleplayerPanel/multiplayerPanel)
    }

    void OnDisable()
    {
      CountDownTimer.Instance.getCurrentTime -= UpdateTimer;

      PlayerUI_InfoUpdater.Instance.getOpponentScore -= UpdateEnemyScore;

      PlayerUI_InfoUpdater.Instance.getLocalPlayer_Name -= UpdateYourName;
      PlayerUI_InfoUpdater.Instance.getLocalPlayer_Avatar -= UpdateYourAvatar;

      PlayerUI_InfoUpdater.Instance.getOpponentPlayer_Name -= UpdateEnemyName;
      PlayerUI_InfoUpdater.Instance.getOpponentPlayer_Avatar -= UpdateEnemyAvatar;
    }

    public void UpdateYourName(string yourName)
    {
      if (GameModeProvider.modeType == GameModeType.Singleplayer)
      {
        sp_localPlayer_Name_Text.text = yourName.ToString();
      }
      else if (GameModeProvider.modeType == GameModeType.Multiplayer)
      {
        mp_localPlayer_Name_Text.text = yourName.ToString();
      }
    }

    public void UpdateYourAvatar(Sprite yourAvatarSprite)
    {
      if (GameModeProvider.modeType == GameModeType.Singleplayer)
      {
        sp_localPlayer_Avatar_Image.sprite = yourAvatarSprite;
      }
      else if (GameModeProvider.modeType == GameModeType.Multiplayer)
      {
        mp_localPlayer_Avatar_Image.sprite = yourAvatarSprite;
      }
    }

    public void UpdateYourScore(int yourScore)
    {
      if (GameModeProvider.modeType == GameModeType.Singleplayer)
      {
        sp_localPlayer_Score_Text.text = yourScore.ToString();
      }
      else if (GameModeProvider.modeType == GameModeType.Multiplayer)
      {
        mp_localPlayer_Score_Text.text = yourScore.ToString();
      }
    }

    public void UpdateEnemyName(string enemyName)
    {
      opponent_Name_Text.text = enemyName.ToString();
    }

    public void UpdateEnemyAvatar(Sprite enemyAvatar)
    {
      opponent_Avatar_Image.sprite = enemyAvatar;
    }

    public void UpdateEnemyScore(int enemyScore)
    {
      opponent_Score_Text.text = enemyScore.ToString();
    }

    public void UpdateTimer(string currentTime)
    {
      if (GameModeProvider.modeType == GameModeType.Singleplayer)
      {
        sp_timer_Text.text = currentTime;
      }
      else if (GameModeProvider.modeType == GameModeType.Multiplayer)
      {
        mp_timer_Text.text = currentTime;
      }
    }

    public void UpdateYourLives(int remainingLives)
    {
      if (GameModeProvider.modeType == GameModeType.Singleplayer)
      {
        singleplayer_heartPanel.transform.GetChild(remainingLives).GetChild(0).gameObject.SetActive(false);
      }
      else if (GameModeProvider.modeType == GameModeType.Multiplayer)
      {
        multiplayer_heartPanel.transform.GetChild(remainingLives).GetChild(0).gameObject.SetActive(false);
      }
    }

    public void ShowGameOverPanel(int finalScore)
    {
      if (GameModeProvider.modeType == GameModeType.Singleplayer)// If we are playing in Singleplayer mode
      {
        gameOverPanel.SetActive(true);
        sp_localplayer_FinalScore_Text.text = finalScore.ToString();
      }
    }

    // Show game mode panel (singleplayerPanel/multiplayerPanel) as per selected GameModeType
    private void OpenGameModePanel()
    {
      if (GameModeProvider.modeType == GameModeType.Singleplayer)
      {
        singleplayerPanel.SetActive(true);
      }
      else if (GameModeProvider.modeType == GameModeType.Multiplayer)
      {
        multiplayerPanel.SetActive(true);
      }
    }

  }
}
