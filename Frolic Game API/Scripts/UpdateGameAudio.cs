using UnityEngine;
using UnityEngine.UI;

namespace FrolicGamesAPI
{
  // A class to pause game audio when app goes in background and unpause game audio when app comes back in focus
  public class UpdateGameAudio : MonoBehaviour
  {
    [SerializeField] private Button m_soundButton;
    [SerializeField] private Image m_soundIconPlaceholder;
    [SerializeField] private Sprite m_soundOnSprite;
    [SerializeField] private Sprite m_soundOffSprite;

    bool isAudioOn;

    void Start()
    {
      UpdateSoundUI();
      if (m_soundButton != null)
      {
        m_soundButton.onClick.AddListener(() => ToggleSound());
      }
      else
      {
        Debug.Log($"[UpdateGameAudio] m_Button field is empty", this);
      }
    }

    //For Local Testing Only
#if UNITY_EDITOR
    private void Update()
    {
      if (Input.GetKeyDown(KeyCode.A))// To toggle game sound app ON/OFF
      {
        ToggleSound();
      }
    }
#endif

    public void ToggleSound(int state = -1)
    {
      if (m_soundButton == null)
      {
        Debug.Log($"[UpdateGameAudio] m_Button field is empty", this);
        return;
      }

      switch (state)
      {
        case -1: // Called by m_soundButton click event
          if (AudioListener.volume == 1)// If volume is on
          {
            isAudioOn = false;
            AudioListener.volume = 0;
            SetSoundOffUI();
          }
          else if (AudioListener.volume == 0)// If volume is off
          {
            isAudioOn = true;
            AudioListener.volume = 1;
            SetSoundOnUI();
          }
          break;
        case 0: // 0 when app comes back in focus
          if (isAudioOn)
          {
            AudioListener.volume = 1;
          }
          else
          {
            AudioListener.volume = 0;
          }
          break;
        case 1: // 1 when app goes in background 
          AudioListener.volume = 0;
          break;
      }

    }

    //Update Sound UI on Scene Load
    private void UpdateSoundUI()
    {
      if (AudioListener.volume == 0)
      {
        isAudioOn = false;
        SetSoundOffUI();
      }
      else
      {
        isAudioOn = true;
        SetSoundOnUI();
      }
    }

    //Set Sound ON UI
    private void SetSoundOnUI()
    {
      if (m_soundIconPlaceholder)
      {
        if (m_soundOffSprite != null)
        {
          m_soundIconPlaceholder.sprite = m_soundOnSprite;
        }
      }
      else
      {
        m_soundButton.GetComponent<Image>().sprite = m_soundOnSprite;
      }
    }

    //Set Sound Off UI
    private void SetSoundOffUI()
    {
      if (m_soundIconPlaceholder)
      {
        if (m_soundOffSprite != null)
        {
          m_soundIconPlaceholder.sprite = m_soundOffSprite;
        }
      }
      else
      {
        m_soundButton.GetComponent<Image>().sprite = m_soundOffSprite;
      }
    }
  }
}

