﻿using UnityEngine;

namespace FrolicGamesAPI
{
    // A class for setting game mode (used just for debugging has no effect in final build)
    // A common use case can be to check if correct ui panel shows as per game mode selected in inspector 
    public class GameModeSetterDebug : MonoBehaviour
    {
#if UNITY_EDITOR
        public GameModeType modeType = GameModeType.Singleplayer;
        void Awake()
        {
            if(modeType == GameModeType.Singleplayer)
            {
                GameModeProvider.modeType = GameModeType.Singleplayer;
            }
            else if(modeType == GameModeType.Multiplayer)
            {
                GameModeProvider.modeType = GameModeType.Multiplayer;
            }
        }
#endif
    }
}
